locals {
  cloud_id    = "b1g5h8bnpf186kpjn51h"
  folder_id   = "b1g3m2mal6jgrejluocf"
  sa_name     = "k8s-cluster-manager-b1g3m2mal6jgrejluocf"
}

resource "yandex_vpc_network" "momo-net" {
  name = "momo-net"
}

resource "yandex_vpc_subnet" "momo-subnet" {
  name = "momo-k8s-master-subnet"
  v4_cidr_blocks = ["10.128.0.0/24"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.momo-net.id
}

resource "yandex_iam_service_account" "momo-k8s-manager" {
  name        = local.sa_name
  description = "K8S zonal service account"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
  # Сервисному аккаунту назначается роль "k8s.clusters.agent".
  folder_id = local.folder_id
  role      = "editor"
  members = [
    "serviceAccount:${yandex_iam_service_account.momo-k8s-manager.id}"
  ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
  # Сервисному аккаунту назначается роль "container-registry.images.puller".
  folder_id = local.folder_id
  role      = "container-registry.images.puller"
  members = [
    "serviceAccount:${yandex_iam_service_account.momo-k8s-manager.id}"
  ]
}

resource "yandex_kubernetes_cluster" "momo-k8s" {
  name = "momo-k8s"
  network_id = yandex_vpc_network.momo-net.id
  
  master {
    version = "1.22"
    zonal {
      zone      = yandex_vpc_subnet.momo-subnet.zone
      subnet_id = yandex_vpc_subnet.momo-subnet.id
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }
  }

  cluster_ipv4_range = "10.223.0.0/16"
  service_ipv4_range = "10.51.0.0/16"

  service_account_id      = yandex_iam_service_account.momo-k8s-manager.id
  node_service_account_id = yandex_iam_service_account.momo-k8s-manager.id
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.editor,
    yandex_resourcemanager_folder_iam_binding.images-puller
  ]

}


resource "yandex_kubernetes_node_group" "momo-k8s-node-group" {
  name = "momo-k8s-node-group"
  cluster_id  = "${yandex_kubernetes_cluster.momo-k8s.id}"
  version     = "1.22"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = true
      subnet_ids         = ["${yandex_vpc_subnet.momo-subnet.id}"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    container_runtime {
      type = "docker"
    }
  }

  scale_policy {
    auto_scale {
      initial = 1
      max = 3
      min = 0
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }
  }
}

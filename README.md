# momo-store.boryan.site aka Пельменная №2

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Устройство репозитория.

1. ```backend``` и ```frontend``` - папки с кодом бэкенда и фронтенда.
2. ```infrastructure/terraform``` - файл terraform с описанием сети и кластера kubernetes.
3. ```infrastructure/kubernetes``` - Kubernetes-манифесты и helm-чарты приложения.

## Релизный цикл приложения и правила версионирования.

Разработка приложения ведется по модели Gitflow, версии всех артефактов указываются вручную.

1. Ветка ```develop``` - интеграционная для новых функций. Тестирование кода проводится в ```sonarqube```. После коммита в эту ветку в ```Gitlab Container Registry``` закачиваются Docker-образы фронтенда и бэкенда.
2. В ветке ```release``` формируется helm чарт для деплоя в кластер kubernetes. Helm-чарт хранится в ```Nexus```.
3. В ветке ```master``` хранится официальная история релиза. В ней выполняется только Job деплоя приложения в кластер kubernetes. Актуальная версия чарта для прода указана в ```Gitlab CI/CD Variables```.

## Локальный запуск приложения

1. Скачать образы из registry

```bash
docker pull gitlab.praktikum-services.ru:5050/std-009-014/momo-store/momo-frontend:<tag>
docker pull gitlab.praktikum-services.ru:5050/std-009-014/momo-store/momo-backend:<tag>
```
2. Создать сеть для контейнеров

```bash
docker network create -d bridge momo_network 
```

3. Запустить контейнеры

```bash
docker run --rm -d --name momo-backend --network=momo_network momo-backend:<tag>
docker run --rm -d --name momo-frontend --network=momo_network -p 80:80 momo-frontend:<tag>
```

## Infrastructure
1. Настроить ```yc``` для работы с облаком.

```bash
yc init
```

2. Настроить CLI для работы от имени сервисного аккаунта

```bash
yc iam key create \
  --service-account-id <идентификатор_сервисного_аккаунта> \
  --folder-name <имя_каталога_с_сервисным_аккаунтом> \
  --output key.json

yc config profile create <имя_профиля>
yc config set service-account-key key.json
yc config set cloud-id <идентификатор_облака>
yc config set folder-id <идентификатор_каталога>  

export YC_TOKEN=$(yc iam create-token)
export YC_CLOUD_ID=$(yc config get cloud-id)
export YC_FOLDER_ID=$(yc config get folder-id)
```

3. Настроить доступ к ```Yandex Object Storage```

```bash
export AWS_ACCESS_KEY_ID=<Идентификатор ключа доступа>
export AWS_SECRET_ACCESS_KEY=<Секретный ключ>
```

4. Развернуть инфраструктуру в Yandex Cloud с помощью ```terraform```. Состояние Terraform'а хранится в ```S3```.

```bash
terraform apply
```

5. Настроить ```kubectl``` для работы с кластером

```bash
yc managed-kubernetes cluster \
  get-credentials momo-k8s \
  --external
```

6. Установить ```ingress-controller``` в кластер ```kubernetes```

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx && \
helm repo update && \
helm install ingress-nginx ingress-nginx/ingress-nginx
```

7. Добавить A-запись с адресом ingress в зоне управления своего домена.

```bash
kubectl get svc
```

8. Установить VPA в кластер ```kubernetes```

```bash
git clone https://github.com/kubernetes/autoscaler.git && \
cd autoscaler/vertical-pod-autoscaler/hack && \
./vpa-up.sh
```

9. Создать файл kubeconfig и сертификат ca.pem для деплоя, затем закодировать их в base64 и добавить в gitlab.

```bash
CLUSTER_ID=<ID кластера kubernetes>

yc managed-kubernetes cluster get --id $CLUSTER_ID --format json | \
  jq -r .master.master_auth.cluster_ca_certificate | \
  awk '{gsub(/\\n/,"\n")}1' > ca.pem

kubectl create -f ./infrastructure/kubernetes/manifest/sa.yaml

SA_TOKEN=$(kubectl -n kube-system get secret $(kubectl -n kube-system get secret | \
  grep admin-user | \
  awk '{print $1}') -o json | \
  jq -r .data.token | \
  base64 --d)

MASTER_ENDPOINT=$(yc managed-kubernetes cluster get --id $CLUSTER_ID \
  --format json | \
  jq -r .master.endpoints.external_v4_endpoint)

kubectl config set-cluster sa-test2 \
  --certificate-authority=ca.pem \
  --server=$MASTER_ENDPOINT \
  --kubeconfig=test.kubeconfig

kubectl config set-credentials admin-user \
  --token=$SA_TOKEN \
  --kubeconfig=test.kubeconfig

kubectl config set-context default \
  --cluster=sa-test2 \
  --user=admin-user \
  --kubeconfig=test.kubeconfig

kubectl config use-context default \
  --kubeconfig=test.kubeconfig
```

## Логирование и мониторинг

1. Создать ```namespace``` для мониторинга

```bash
kubectl create ns monitoring
```

2. Установить Loki

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm upgrade --install --namespace monitoring loki grafana/loki
```

3. Установить Prometheus и Grafana

```bash
helm repo add prometheus https://prometheus-community.github.io/helm-charts
helm repo update
helm upgrade --install --namespace monitoring prometheus prometheus-community/prometheus
helm upgrade --install --namespace monitoring grafana prometheus-community/kube-prometheus-stack
```